/**
 * Esta clase permite al usuario controlar el TimeOut de la aplicación,
 * A tal modo que evita la desconexión de la aplicación a los servicios
 * Usados en la aplicación realizando una consulta cualquiera a los servicios
 *                       --------------------------
 * INSTANCIACIÓN DE LA LIBRERÍA
 * 
 * En el controlador donde vamos a instanciar la librería (Worklist-Master)
 * Vamos a instanciar la carpeta donde se encuentra alojada Ej:
 * sap.ui.define([
 *    "com/velez/galena/app/libs/AppRefresh",
 * ], function(AppRefresh) { ... });
 * 
 * En este ejemplo: "com/velez/galena/app" es el namespace de la aplicación
 * "libs" es el nombre de la carpeta donde está contenida la librería
 * Y "AppRefresh" es el nombre de esta librería; recordemos agregarlo
 * En los parámetros de la función "AppRefresh".
 * 
 * Ahora solo falta llamar a la librería llamando a su método "initialization".
 * 
 * NOTAS:
 * Se le proporciona al usuario una función llamada "customFunction" que le
 * permite realizar los procesos que requiera para ejecutar exitósamente
 * la consulta hacia el servicio.
 *                       --------------------------
 * REQUERIMIENTOS POR PARTE DEL DESARROLLADOR
 * Para que esta librería funcione correctamente se necesitan los strings
 * En los siguientes métodos de la clase:
 * 
 * Método sServiceUrl:
 * La URL de consumo de servicios de la aplicación.
 * 
 * Método sEntity:
 * La entidad que se va a consumir.
 * 
 * Método oGlobalFilter:
 * El string de consulta que retornará pocos resultados o no traerá.
 *                       --------------------------
 * @returns {Object} Clase SAPUI5 para uso de código de barras
 * @author Daniel López Echavarría
*/
//Varible de control de tiempo
var idleTime, sMinutos, sSegundos;

sap.ui.define([
	"sap/m/Dialog",
	"sap/m/DialogType",
	"sap/m/Text",
	"sap/m/Button"
	], function(Dialog, DialogType, Text, Button) {
	"use strict";
	return {
		/**
		 * URL del Servicio
		 * para las consultas
		 * @private
		*/
		sServiceUrl: "/SAP_Gateway/odata/SAP/ZSD_CATALOGO_PRODUCTO_SRV/",
		/**
		 * Nombre de la entidad cualquiera a usar
		 * como búsqueda que traiga 0 o pocos registros.
		 * @private
		*/
		sEntity: "/TimeOutSet(Idioma='S')",
		/**
		 * Filtro que se le va a aplicar a la entidad para
		 * que traiga los registros
		 * @private
		*/
		oGlobalFilter: null,
		/**
		 * Función personalizada para el usuario
		 * @private
		*/
		customFunction:function() {
			return;
		},
		/**
		 * Limite máximo del timeout
		 * @private
		*/
		timeout: 15,
		/**
		 * Minutos de inicio de la cuenta regresiva.
		 * @private
		*/
		countStart: 3,
		/**
		 * Intervalo de milesimas, donde se va a realizar
		 * La cuenta principal. Se deja determinado 60000
		 * Como determinación de que el intervalo se va a
		 * Realizar cada minuto.
		 * @private
		*/
		timeInterval: 60000,
		/**
		 * Definición del intervalo del timeout
		 * @private
		*/
		timeoutInterval: 1000,
		/**
		 * Inicialización del proceso de TimeOut Controlado
		 * @param {String} sService - Url del Servicio
		 * @param {String} sEntity - Entidad a usar como ayuda
		 * @public
		*/
		initialization: function() {
			//Definimos that como this
			var that = this;
			var Console = window.console;
			//Inicializamos las variables globales
			idleTime = 0;
			sMinutos = that.countStart;
			sSegundos = 0;
			//Espacio personalizado para el usuario
			this.customFunction();
			//Definimos función de reinicio
			function timerIncrement() {
				idleTime += 1;
				if (idleTime === that.timeout - 10) {
					Console.log( that.onCleanSearch().Fecha );
				}
				if (idleTime === that.timeout - that.countStart) {
					that.fnAbrirDialogo();
				}
			}
			// Función de auto ejecucicón para el contador
			$(document).ready(function() {
				//Increment the idle time counter every minute.
				setInterval(timerIncrement, that.timeInterval);
				//Zero the idle timer on click event.
				$(this).click(function(e) {
					idleTime = 0;
				});
				$(this).keypress(function(e) {
					idleTime = 0;
				});
			});
		},
		/**
		 * Función que abre el dialogo de reinicio
		 * y la cuenta regresiva.
		 * @private
		*/
		fnAbrirDialogo: function() {
			var that = this;
			var Console = window.console;
			that._countDown = new Dialog({
				type: DialogType.Standard,
				title: "Reinicio por inactividad",
				contentWidth: "500px",
				content: [
					new Text({
						text: "Se ha iniciado la cuenta regresiva para el reinicio de la aplicacion por inactividad, haga click en continuar para eliminar el conteo."
					}),
					new Text("lblRestartText", {
						text: "\nSe reiniciará en "
					})
				],
				buttons: [
					new Button({
						text:"Reiniciar",
						type:"Default",
						width:"48%",
						press:function() {
							window.location.reload();
						}
					}),
					new Button({
						text:"Continuar",
						width:"48%",
						type:"Accept",
						press:function() {
							//Reiniciamos los contadores
							sSegundos = 0;
							idleTime = 0;
							sMinutos = that.countStart;
							//Hacemos llamado a entidad
							Console.log( that.onCleanSearch().Fecha );
							//Cerramos el dialogo
							that._countDown.close();
						}
					})
				],
				afterOpen: [that.counterStart, that],
				afterClose: function() {
					that._countDown.destroy();
				}
			}).addStyleClass("sapUiContentPadding");
			that._countDown.open();
		},
		/**
		 * Método que inicia la cuenta regresiva final de 3 minutos
		 * que se ejecuta en el dialogo.
		 * @private
		*/
		counterStart:function() {
			var that = this;
			setInterval(function() {
				//Tratamos el contador
				if (idleTime >= that.timeout - that.countStart) {
					if (sMinutos === 0 && sSegundos === 0) {
						//Si los segundos y los minutos están en 0's reiniciar la app
						window.location.reload();
					} else {
						// Jugamos con los minutos y segundos
						if (sSegundos === 0) {
							//Disminuimos los minutos
							sMinutos -= 1;
							sSegundos = 59;
						} else {
							//Disminuimos los segundos
							sSegundos -= 1;
						}
						//Actualizamos el titulo
						that.updateTitle();
					}
				}
			}, that.timeoutInterval);
			//Actualizamos el titulo
			that.updateTitle();
		},
		/**
		 * Obtener el control del label que muestra el counter.
		 * @returns {Object} Control del Label
		 * @private
		*/
		getLabelControl:function() {
			var oLmnt = sap.ui.getCore().byId("lblRestartText");
			return oLmnt;
		},
		/**
		 * Método que ayuda a actualizar el timer
		 * cada segundo.
		 * @private
		*/
		updateTitle:function() {
			this.getLabelControl().setText("\nSe reiniciará en " + sMinutos + ":" + sSegundos);
		},
		/**
		 * Realizar la lectura de la entidad PRODUCTOS_DISPONIBLES
		 * @param {String} data Data to be filtered
		 * @returns {Object} hierarchy name
		 * @private
		 */
		onCleanSearch: function() {
			var sService = this.sServiceUrl;
			var sEntidad = this.sEntity;
			var oFilters = this.oGlobalFilter;
			var oModel = new sap.ui.model.odata.ODataModel(sService, true);
			var oResponse = this.fnReadEntity(oModel, sEntidad, oFilters);
			return oResponse;
		},
		/**
		 * Función que lee entidades
		 * @param {sap.ui.Model} oModelo Modelo sapui5
		 * @param {String} sEntidad Entidad a leer
		 * @param {Object} oFilters Object with filters data
		 * @returns {Object} Data resulting after read
		 * @private
		 */
		fnReadEntity: function(oModelo, sEntidad, oFilters) {
			var mensaje;
			var fnSuccess = function(data, response) {
				mensaje = data;
			};
			var fnError = function(e) {
				mensaje = e.message;
			};
			oModelo.read(sEntidad, null, oFilters, false, fnSuccess, fnError);
			return mensaje;
		}
	};
});