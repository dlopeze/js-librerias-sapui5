/**
 * Librería de Funciones estándar SAPUI5
 *
 * INSTANCIACIÓN DE LA LIBRERÍA
 *  
 * En este ejemplo: "com.velez.ingresomercancia" es el namespace de la aplicación.
 * Y "GeneralController" es el nombre de esta librería.
 * 
 * Para que la aplicacion reconozca este controlador se necesita realizar lo siguiente:
 * Abrimos el "BaseController.controlles.js" e instanciamos este controlador:
 * 
 * RECONOCE: Este es un ejemplo de como normalmente en BaseController se instancia
 * sap.ui.define([
 *     "sap/ui/core/mvc/Controller"
 * ], function(Controller) {
 *     return Controller.extend("com.velez.ingresomercancia.controller.BaseController", {
 * }
 * 
 * Ahora, lo que necesitas modificar en el BaseController son las siguientes líneas:
 * sap.ui.define([
 *     "com.velez.ingresomercancia.controller.GeneralController"                                 <-- Llamamos la librería GeneralController
 * ], function(GeneralController) {                                                              <-- Instanciamos en los parámetros de función
 *     return GeneralController.extend("com.velez.ingresomercancia.controller.BaseController", { <-- Agregamos el parámetro nuevo
 * }
 * 
 * 
 * NOTA: Este es un controlador como lo es el BaseController, asi que si necesitas llamar una función dentro de esta
 * Librería, solo hace falta llamarla como cualquier método. Ejemplo: this.getApiUser();
 * 
 * @returns {Object} General Base Controller
 * @public
 * @Author Daniel López Echavarría
 */

//Espacio para declaración de variables globales

sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/odata/ODataModel",
    "sap/ui/core/format/DateFormat",
    "sap/ui/Device",
    "sap/m/Button",
    "sap/m/Dialog",
    "sap/m/MessageBox",
    "sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function(Controller, JSONModel, ODataModelV1, DateFormat, Device, Button, Dialog, MessageBox, Filter, FilterOperator) {
    "use strict";
    return Controller.extend("com.compañia.nombre_app.controller.GeneralController", {

        /**
         * Función que devuelve el nombre y correo logueado
         * @param {Integer} iType | Tipo operación: 1: Modelo, 2: Retornar
         * @returns {Object} Información de sessión
         * @private
         */
        getApiUser: function(iType) {
            //Create a JsonModel
            var userModel = new JSONModel();
            //Call User Api Functions
            userModel.loadData("/services/userapi/currentUser", null, false);
            //Añadir a un modelo (opcional)
            if (iType === 1) {
                this.setModel(userModel, "userapi");
                //Return Data
                return {};
            } else {
                //Get Data
                var oData = userModel.getData();
                //Return Data
                return oData;
            }
        },

        /**
         * Función que Oculta la vista Master
         * 
         * @version SAPUI5<1.60
         * @private
         */
        onHideMaster: function() {
            if (Device.system.desktop) {
                this.getView().getParent().getParent().setMode("HideMode");
            }
        },

        /**
         * Función que muestra la vista Master
         * 
         * @version SAPUI5<1.60
         * @private
         */
        onShowMaster: function() {
            if (Device.system.desktop) {
                this.getView().getParent().getParent().setMode("ShowHideMode");
            }
        },

        /**
         * Función de lectura de entidades, require
         * @param {sap.ui.Model} oModelo | Modelo sapui5
         * @param {String} sEntidad      | Entidad a leer
         * @param {Object} oFilters      | Object with filters data
         * @returns {Object} Data resulting after read
         * @private
         */
        readData: function(oModelo, sEntidad, oFilters) {
            var mensaje;
            var fnSuccess = function(data) {
                mensaje = data;
            };
            var fnError = function(e) {
                mensaje = JSON.parse(e.response.body);
                mensaje = mensaje.error.message.value;
                mensaje = "Net Error: " + mensaje;
            };
            oModelo.read(sEntidad, null, oFilters, false, fnSuccess, fnError);
            return mensaje;
        },

        /**
         * Consumir servicio CREATE.
         * @public
         * @param {object} sModelo      | URL Servicio Odata (sap.opu.odata.sap.ZGW_***_SRV)
         * @param {string} sEntidad     | Entidad del servicio que se va a consumir
         * @param {object} oDatoEndidad | Hace referencia a los dato a enviar a la entidad
         * @returns {string} mensaje
         */
        createData: function(sModelo, sEntidad, oDatoEndidad) {
            var oModelo = new ODataModelV1(sModelo, { json: true });
            var vMensaje = null;
            var oMensaje = {};
            var fnSucess = function(data) {
                oMensaje.tipo = "S";
                oMensaje.msjs = "OK";
                oMensaje.datos = data;
            };
            var fnError = function(e) {
                vMensaje = JSON.parse(e.response.body);
                vMensaje = vMensaje.error.message.value;
                oMensaje.tipo = "E";
                oMensaje.msjs = vMensaje;
            };
            oModelo.create(sEntidad, oDatoEndidad, null, fnSucess, fnError, false);
            return oMensaje;
        },

        /**
         * Consumir servicio UPDATE.
         * @public
         * @param {object} oModelo hace referencia al modelo del servicio
         * @param {string} sEntidad hace referencia a la entidad que se va a consumir
         * @param {object} pDatoEndidad hace referencia a los dato a enviar a la entidad
         * @returns {string} mensaje
         */
        updateData: function(oModelo, sEntidad, pDatoEndidad) {
            var vMensaje = null;
            var oMensaje = {};
            var that = this;
            var fnSucess = function(data, response) {
                oMensaje.tipo = "S";
                oMensaje.response = response;
            };
            var fnError = function(e) {
                oMensaje.tipo = "E";
                try {
                    vMensaje = that.fnErrorBack(e.response.body);
                    oMensaje.msjs = vMensaje;
                } catch (err) {
                    oMensaje.msjs = "Error inesperado, comuniquese con el administrador del sistema";
                }
            };
            oModelo.update(sEntidad, pDatoEndidad, null, fnSucess, fnError, false);
            return oMensaje;
        },

        /**
         * Consumir servicio UPDATE.
         * @public
         * @param {object} oModel hace referencia al modelo del servicio
         * @param {string} sEntity hace referencia a la entidad donde se van a eliminar los datos
         * @returns {string} mensaje
         */
        deleteData: function(oModel, sEntity) {
            var that = this,
                vMensaje = null,
                oMensaje = {};
            // Si es exitoso el envio de los datos
            var fnSucess = function(data, response) {
                oMensaje.tipo = "S";
                oMensaje.response = response;
            };
            var fnError = function(e) {
                oMensaje.tipo = "E";
                try {
                    vMensaje = that.fnErrorBack(e.response.body);
                    oMensaje.msjs = vMensaje;
                } catch (err) {
                    oMensaje.msjs = "Error inesperado, comuniquese con el administrador del sistema.";
                }
            };
            oModel.remove(sEntity, null, fnSucess, fnError, false);
            return oMensaje;
        },

        /**
         * Formatear y obtener cada uno de los mensajes que provienen desde el servicio  
         * @param {String} pError | Hace referencia al mensaje  mensajes que retorno la entidad
         * @public
         */
        fnErrorBack: function(pError) {
            var sMensaje = "";
            if (pError.indexOf("?xml") > -1) { // <-- Validamos si es XML
                try {
                    var oParser = new DOMParser();
                    var xmlDoc = oParser.parseFromString(pError, "text/xml");
                    sMensaje = xmlDoc.getElementsByTagName("message")[0].innerHTML;
                } catch (err) {
                    jQuery.sap.log.error(err);
                }
            } else { //<--- Validamos JSON
                try {
                    var oMessage = JSON.parse(pError);
                    var aErrores = oMessage.error.innererror.errordetails;
                    for (var i = 0; i < aErrores.length; i++) {
                        if (aErrores[i].code !== "/IWBEP/CX_SD_GEN_DPC_BUSINS") {
                            sMensaje += aErrores[i].message + "\n\n";
                        }
                    }
                    //Mensaje Default si no encuentra nada
                    if (sMensaje === "") {
                        sMensaje = oMessage.error.message.value;
                    } else {
                        //Eliminamos salto de más
                        sMensaje = sMensaje.substring(0, sMensaje.length - 4);
                    }
                } catch (err) {
                    jQuery.sap.log.error(err);
                }
            }
            //Mostramos el mensaje de error en pantalla
            MessageBox.information(sMensaje);
        },

        /**
         * Obtener el control de un elemento de la vista
         * @param {String} sId - Id del elemento a obtener el control
         * @returns {Object} Objeto con el control del elemento
         * @private
         */
        getControl: function(sId) {
            var oLmnt = this.byId(sId);
            if (!oLmnt) {
                oLmnt = sap.ui.getCore().byId(sId);
            }
            return oLmnt;
        },

        /**
         * Función utilizada para obtener el control de una vista diferente a la actual
         * Esta función depende de la función del BaseController getRouter
         * getRouter : function () { return this.getOwnerComponent().getRouter(); },
         * getRouter: function() { return sap.ui.core.UIComponent.getRouterFor(this); },
         * Ejemplo: 
         * - Obtener la vista Master desde la vista Detail.
         * - Obtener la vista Worklist desde la vista Object.
         * @param {String} sPathVista - Ruta completa de la vista SAPUI5
         * @returns {sap.ui.core.mvc.View} Objeto de la vista llamada
         */
        fnObtenerVista: function(sPathVista) {
            var sVista = sPathVista;
            var oObject = this.getRouter().getView(sVista);
            return oObject;
        },

        /**
         * Obtener variable de datos globales
         * @returns {Object} Objeto
         * @private
         */
        getGlobal: function() {
            if (!sap.ui.getCore().oGlobal) {
                sap.ui.getCore().oGlobal = {};
            }
            return sap.ui.getCore().oGlobal;
        },

        /**
         * Crear hora para envio al ERP
         * @param {String} dDate - Fecha tipo Date()
         * @returns {String} Hora para el ERP YYYY-MM-DDT00:00:00
         */
        getSapDate: function(dDate) {
            var oDateFormat = DateFormat.getDateTimeInstance({
                pattern: "yyyy-MM-ddT00:00:00"
            });
            return oDateFormat.format(dDate);
        },

        /**
         * Elimina todas las letras del parametro y devuelve solo numeros
         * @public
         * @param {string} pDigitos cadena a tratar
         * @return {string} sDigitos cadena con solo numeros
         */
        fnSoloNumeros: function(pDigitos) {
            var sDigitos = pDigitos.replace(/[\D\s\._\-]+/g, "");
            return sDigitos;
        },

        /**
         * Dialogo que indica al usuario la ejecución de un proceso
         * NOTA: En la función especificada debe cerrarse el BusyDialogo así <<< this._busyDialog.close(); >>>
         * @param {function} fnAfterOpen Es la función que se quiere llamar desde el metodo afterOpen del Dialog.
         * @param {object} oParameters Son los parametros que se desean enviar a la funcion metodo afterOpen.
         * @private
         */
        showBusyDialog: function(fnAfterOpen, oParameters) {
            var that = this;
            that._busyDialog = new Dialog({
                type: DialogType.Standard,
                title: "Procesando",
                content: new sap.m.HBox({
                    justifyContent: "Center",
                    alignItems: "Center",
                    items: new sap.m.BusyIndicator({
                        text: ".:. Solo serán unos segundos .:.",
                        sClassName: "sapUiSmallMarginBottom"
                    })
                }),
                afterOpen: [oParameters, fnAfterOpen, that], //[Parametros, FuncionEjecutar, ContextoGlobal]
                afterClose: function() {
                    that._busyDialog.destroy();
                    that._busyDialog = null;
                }
            });
            that._busyDialog.open();
        },

        /**
         * Cerrar Dialogo
         * @private
         */
        closeBusyDialog: function() {
            this._busyDialog.close();
        },

        /**
         * Crear y abrir el fragment número 1.
         * @param {String} url - view path on Galena Project
         * @private
         */
        createPrincipalFragment: function(url) {
            if (!this._oDialog) {
                this._oDialog = sap.ui.xmlfragment(url, this);
            }
            this.getView().addDependent(this._oDialog);
            jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
            this._oDialog.open();
        },

        /**
         * Cerrar Dialogo #1
         * @private
         */
        closePrincipalFragment: function() {
            try {
                this._oDialog.close();
            } catch (e) {
                jQuery.sap.log.info("Something has happened!");
            }
            this._oDialog.destroy();
            this._oDialog = null;
        },

        /**
         * Crear y abrir el fragment número 2.
         * @param {String} url - view path on Galena Project
         * @private
         */
        createSecondaryFragment: function(url) {
            if (!this._oDialogo) {
                this._oDialogo = sap.ui.xmlfragment(url, this);
            }
            this.getView().addDependent(this._oDialogo);
            jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogo);
            this._oDialogo.open();
        },

        /**
         * Cerrar Dialogo #2
         * @private
         */
        closeSecondaryFragment: function() {
            try {
                this._oDialogo.close();
            } catch (e) {
                jQuery.sap.log.info("Something has happened!");
            }
            this._oDialogo.destroy();
            this._oDialogo = null;
        },

        /**
         * Función que filtra datos por el parámero y el valor de consulta
         * @param {Array} aArray - Array de objetos para realizar comparación
         * @param {String} sCampo - Campo del array de objetos a comparar
         * @param {String} sValor - String con el valor a comparar
         * @returns {Array} Array con los datos que hacen match
         * @private
         */
        setDataFilter: function(aArray, sCampo, sValor) {
            function filtrarDatos(obj) {
                if (obj[sCampo] === sValor) {
                    return true;
                }
            }
            return aArray.filter(filtrarDatos);
        },

        /**
         * Ordenar array por un campo dado ascendentemente
         * @param {Array} aData - Array de datos a organizar
         * @param {String} sProp - propiedad por la cual se va a organizar
         * @return {Array} - Array organizado
         */
        sortDataAscOne: function(aData, sProp) {
            return aData.sort(function(a, b) {
                if (a[sProp] > b[sProp]) {
                    return 1;
                }
                if (a[sProp] < b[sProp]) {
                    return -1;
                }
                return 0;
            });
        },

        /**
         * Ordenar array por un campo dado descendentemente
         * @param {Array} aData - Array de datos a organizar
         * @param {String} sProp - propiedad por la cual se va a organizar
         * @return {Array} - Array organizado
         */
        sortDataDescOne: function(aData, sProp) {
            return aData.sort(function(a, b) {
                if (a[sProp] < b[sProp]) {
                    return 1;
                }
                if (a[sProp] > b[sProp]) {
                    return -1;
                }
                return 0;
            });
        },

        /**
         * Presionar la primera posicion de una lista o tabla
         * Posee dependencia de la función getControl 
         * @param {String} sIdObject Identificador de la lista a tratar
         * @public
         */
        fnPressFirstPosition: function(sIdObject) {
            var oReferenceObject = this.getControl(sIdObject);
            if (oReferenceObject.getItems().length > 0) {
                oReferenceObject.getItems()[0].firePress();
            }
        },

        /**
         * Dialogo de confirmación
         * @public
         * @param {string} sTextMsj       | hace referencia al mensaje de visualización del dialogo
         * @param {string} sTitulo        | Hace referencia al titulo del dialogo
         * @param {string} ofunctionSi    | Función que se ejecutará cuando el usuario presione Si
         * @param {object} p_obj_contexto | Hace referencia al contexto de la apliccion 'this'
         */
        fnApproveDialog: function(sTextMsj, sTitulo, ofunctionSi, oContexto) {
            var that = this;
            this._dialog = new Dialog({
                title: sTitulo,
                type: "Message",
                content: new Text({
                    text: sTextMsj
                }),
                beginButton: new Button({
                    text: "Si",
                    press: [null, ofunctionSi, oContexto]
                }),
                endButton: new Button({
                    text: "No",
                    press: function() {
                        that._dialog.close();
                    }
                }),
                afterClose: function() {
                    that._dialog.destroy();
                    that._dialog = null;
                }
            });
            this._dialog.open();
        },

        /**
         * Realizar el filtro de la data global que vino del ERP
         * @param {String} sIdControl       | Id de Tabla o lista
         * @param {String} sSearchField     | Id del SearchField o Input
         * @param {Array}  aODataFilters    | Lista de Id's de campos del modelo a comparar
         * @param {String} sIdModel         | Id de modelo de la master o Worklist
         * @param {String} sIdi18nListTitle | Id del item i18n donde se realiza el count de items
         * @private
         */
        onSearchList: function(sIdControl, sSearchField, aODataFilters, sIdModel, sIdi18nListTitle) {
            //Definimos datos
            var oList = this.getControl(sIdControl);
            var oSearchField = this.getControl(sSearchField);
            var sFilterPattern = oSearchField.getValue().toLowerCase();
            var aListItems = oList.getItems();
            var bVisibility;
            var iCount = 0;
            //Realizamos filtro uno a uno
            for (var i = 0; i < aListItems.length; i++) {
                bVisibility = this._searchOnODataFields(aListItems[i], sFilterPattern, aODataFilters);
                //Cambiamos la visibilidad
                aListItems[i].setVisible(bVisibility);
                //Aumentamos contador de items activos
                if (bVisibility) {
                    iCount++;
                }
            }
            //Cambiar título de la lista
            if (sIdModel && sIdi18nListTitle) {
                var sTitle = this.getResourceBundle().getText(sIdi18nListTitle, [iCount]);
                this.getModel(sIdModel).setProperty("/title", sTitle);
            }
        },

        /**
         * Evento dependiente de onSearchList para filtrar los elementos de la lista sin ir al ERP
         * El parámetro aODataFilters requiere un array dtrings con los campos del modelo a filtrar
         * Si se quiere filtrar por Material, Detalle y sociedad el array sería
         * Por ejemplo ["Matnr", "Maktx", "Maktg", "Bukrs", "Butxt"]
         * 
         * @param {sap.m.StandardListItem} oItem | Item de lista para realizar comparación
         * @param {String} sFilterPattern        | Texto a filtrar
         * @param {String Array} aODataFilters   | Array de Strings que contienen los campos del modelo Odata/JSON
         * @public
         */
        _searchOnODataFields: function(oItem, sFilterPattern, aODataFilters) {
            //Definimos campos del Modelo Odata para usar para filtrar
            var aODataSearchFields = aODataFilters;
            //oData data, ojo, si el modelo tiene otro nombre agregarlo en getBindingContext('')
            var oModel = oItem.getBindingContext().getProperty();
            //Realizar filtro
            return aODataSearchFields.some(function(sKey) {
                var sValue = oModel[sKey];
                if (typeof sValue === "string") {
                    if (sValue.toLowerCase().indexOf(sFilterPattern) !== -1) {
                        return true;
                    }
                }
            });
        },

        /**
         * Función que sive para eliminar los acentos de las palabras a excepción de las {ñ}
         * @param {String} sTexto | Texto a eliminar el acentos
         * @returns {String} Texto convertido
         * @private
         */
        fnEliminarAcentos: function(sTexto) {
            return sTexto
                .normalize("NFD")
                .replace(/([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi, "$1")
                .normalize();
        },

        /**
         * Función que sirve para capitalizar texto
         * @param {String} sText | Texto a capitalizar
         * @returns {String} Texto Capitalizado
         */
        capitalizarTexto: function(sText) {
            if (sText === null || sText.length === 0) {
                return "";
            }
            var sTexto = sText.toLowerCase();
            return sTexto.replace(/(^|\s)([a-z])/g, function(m, p1, p2) {
                return p1 + p2.toUpperCase();
            });
        },

        /**
         * Realizar formateo a los numeros.
         * @param {Int|String} amount | Numero a convertir
         * @param {Integer} decimals  | Numero de decimales a mostrar - Default: 0
         * @returns {String} Numero formateado
         * @private
         */
        fnNumberFormat: function(amount, decimals) {
            amount += "";
            amount = parseFloat(amount.replace(/[^0-9\.]/g, ""));
            decimals = decimals || 0;
            if (isNaN(amount) || amount === 0) {
                return parseFloat(0).toFixed(decimals);
            }
            amount = "" + amount.toFixed(decimals);
            var amount_parts = amount.split("."),
                regexp = /(\d+)(\d{3})/;
            while (regexp.test(amount_parts[0])) {
                amount_parts[0] = amount_parts[0].replace(regexp, "$1" + "," + "$2");
            }
            return amount_parts.join(".");
        },

        /**
         * Crear Filtros dinámicamente
         * @param {String} sPath Ruta o nombre de la entidad a enviar en Filtro
         * @param {String|null} sOperator Operador con el que se creará el filtro, por default será EQ
         * @param {String|null} sLow Valor #1 del filreo
         * @param {String|null} sHigh Valor #2 del filreo
         * @param {String[]|null} aStrTable Crear un filtro de varios datos
         * @param {String} sPathTable Nombre campo de tabla
         * @returns {sap.ui.model.Filter[]} Filtro SAPUI5
        */
        fnCreateFilter:function(sPath, sOperator, sLow, sHigh, aStrTable, sPathTable) {
            //Creamos variables de trabajo
            var aFilters = [], aStrTbl = [];
            var sOperador = "", sValor1 = "", sValor2 = "";
            //Validamos parámetros de entrada
            sOperador = typeof sOperator !== "object" ?  sOperator : FilterOperator.EQ;
            sValor1   = typeof sLow !== "object" ? sLow : "";
            sValor2   = typeof sHigh !== "object" ?  sHigh : "";
            aStrTbl   = Array.isArray(aStrTable) ? aStrTable : [];
            //Validamos si es un filtro individual
            if (aStrTbl.length > 0) {
                if(!sPathTable) {
                    return aFilters;
                }
                for (var i = 0; i < aStrTbl.length; i++) {
                    aFilters.push(new Filter({
                        path: sPath,
                        operator: sOperador,
                        value1: aStrTbl[i][sPathTable]
                    }));
                }
            }
            //Validamos si se han envido filtros idividuales
            if (sValor1 !== "") {
                aFilters.push(new Filter({
                    path: sPath,
                    operator: sOperador,
                    value1: sValor1,
                    value2: sValor2
                }));
            }
            // Retornamos Objeto Creados
            return aFilters;
        }

    });
});