/**
 * Esta clase permite al usuario agregar un Pop-Up con las funciones estandar
 * definidas en la compañía Cueros Vélez para todas las aplicaciones Fiori.
 *                       --------------------------
 * INSTANCIACIÓN DE LA LIBRERÍA
 * 
 * En el controlador donde vamos a instanciar la librería (BaseController)
 * sap.ui.define([
 *    "com/velez/galena/app/libs/StandardTools",
 * ], function(StandardTools) { ... });
 * 
 * En este ejemplo: "com/velez/galena/app" es el namespace de la aplicación
 * "libs" es el nombre de la carpeta donde está contenida la librería
 * Y "StandardTools" es el nombre de esta librería; recordemos agregarlo
 * En los parámetros de la función "StandardTools".
 * 
 * Ahora creamos un botón y en su propiedad "press" una función cualquiera,
 * dentro de ella llamamos a la librería llamando a su método "openToolsDialog()".
 * Ejemplo:
 * 
 * XML
 * <Button text="Más Opciones" press="fnMasOpciones" />
 * 
 * JS
 * fnMasOpciones:function() {
 *		StandardTools.openToolsDialog();
 * }
 *                       --------------------------
 * REQUERIMIENTOS POR PARTE DEL DESARROLLADOR
 * 
 * Para que esta librería funcione correctamente se necesitan que el desarrollador
 * Agregue en su archivo de estilos "css" las siguientes clases:
 * 
 * .velezButton {background-color: #f7f7f7;border: 1px solid #ababab;padding: 3px 15px;text-align: center;font-size: 16px;margin: 4px 2px;cursor: pointer;border-radius: 0.2rem;}
 * .velezButton div:nth-child(2) {padding-left: 5px;}
 * .velezButton div a {text-decoration: none !important;}
 *                       --------------------------
 * @returns {Object} Clase SAPUI5 para uso de funciones estandard de la empresa Cueros Vélez
 * @author Daniel López Echavarría
 */

sap.ui.define([
	"sap/m/Dialog",
	"sap/m/DialogType",
	"sap/m/Button",
	"sap/m/HBox",
	"sap/m/Link",
	"sap/ui/core/Icon",
	"sap/m/MessageStrip",
	"sap/m/Label",
	"sap/m/Input",
	"sap/m/TextArea",
	"sap/m/VBox",
	"sap/m/MessageToast"
], function(Dialog, DialogType, Button, HBox, Link, Icon, MessageStrip, Label, Input, TextArea, VBox, MessageToast) {
	"use strict";
	return {
		/**
		 * URL del Servicio
		 * para las consultas
		 * @private
		 */
		sServiceUrl: "/SAP_Gateway/odata/SAP/ZSD_CATALOGO_PRODUCTO_SRV/",
		/**
		 * Nombre de la entidad cualquiera a usar
		 * como búsqueda que traiga 0 o pocos registros.
		 * @private
		 */
		sEntity: "/EnvioCorreoSet",
		/**
		 * Abrir Dialogo con las funciones standard
		 * De Cueros Vélez.
		 * Funcionalidad sometida a testing
		 * @public
		 */
		openToolsDialog: function() {
			//Definimos el contexto de la librería
			var that = this;
			//Creamos el dialogo
			this._ToolsDialog = new Dialog({
				title: "Más Opciones",
				content: [
					new Button({
						text: "Déjanos tu Comentario",
						width: "100%",
						icon: "sap-icon://comment",
						press: function() {
							that.dialogSendMessage();
						}
					}),
					new HBox({
						width: "100%",
						alignItems: "Center",
						alignContent: "Center",
						justifyContent: "Center",
						items: [
							new Icon({
								src: "sap-icon://sys-help",
								width: "auto"
							}),
							new Link({
								href: "//web.microsoftstream.com/channel/2dadf1f4-2025-42c7-9f45-546c48bd0285",
								target: "_blank",
								width: "auto",
								text: "Ayuda"
							})
						]
					}).addStyleClass("velezButton")
				],
				type: DialogType.Message,
				buttons: [
					new Button({
						text: "Cerrar",
						press: function() {
							that._ToolsDialog.close();
						}
					})
				],
				afterClose: function() {
					that._ToolsDialog.destroy();
				}
			}); //End Dialog
			this._ToolsDialog.open();
		},
		/**
		 * Función que abre dialogo de envío de correos electrónicos
		 * @private
		 */
		dialogSendMessage: function() {
			//Definimos that como contexto de la clase
			var that = this;
			//Creamos el dialogo
			this._MailDialog = new Dialog({
				title: "Comunicate con nosotros",
				type: DialogType.Standard,
				contentWidth: "500px",
				content: [
					new MessageStrip({
						text: "Habilitamos este espacio para que puedas contarnos, que te gusta de la aplicación, que no te gusta o si tienes algúna idea para que la hagamos realidad. Recuerda que no hay malas ideas!",
						showIcon: true
					}).addStyleClass("sapUiSmallMarginBottom"),
					new Label({
						text: "Ingresa tu correo electrónico",
						width: "100%"
					}),
					new Input("nameMessage", {
						width: "100%",
						type: "Email"
					}),
					new Label({
						text: "Dejanos tu mensaje",
						width: "100%"
					}),
					new TextArea("descriptionMessage", {
						tooltip: "Enviar Mensaje",
						width: "100%",
						rows: 5
					}),
					new Label({
						text: "Califica:",
						width: "100%"
					}),
					new HBox({
						width: "100%",
						items: [
							new VBox({
								width: "33%",
								alignContent: "Center",
								alignItems: "Center",
								items: [
									new Icon("thumbUp", {
										press: [that.onSelectOptionItem, that],
										src: "sap-icon://thumb-up",
										hoverColor: "blue",
										alt: "thumb-up",
										color: "black",
										size: "50px"
									}),
									new Label({
										text: "¡Me gusta!",
										design: "Bold"
									})
								]
							}),
							new VBox({
								width: "33%",
								alignContent: "Center",
								alignItems: "Center",
								items: [
									new Icon("thumbDown", {
										press: [that.onSelectOptionItem, that],
										src: "sap-icon://thumb-down",
										hoverColor: "blue",
										alt: "thumb-down",
										color: "black",
										size: "50px"
									}),
									new Label({
										text: "¡No me gusta!",
										design: "Bold"
									})
								]
							}),
							new VBox({
								width: "33%",
								alignContent: "Center",
								alignItems: "Center",
								items: [
									new Icon("lightbulb", {
										press: [that.onSelectOptionItem, that],
										src: "sap-icon://lightbulb",
										hoverColor: "blue",
										alt: "lightbulb",
										color: "black",
										size: "50px"
									}),
									new Label({
										text: "¡Tengo una idea!",
										design: "Bold"
									})
								]
							})
						]
					})
				],
				buttons: [
					new Button("btnCloseMessages", {
						press: function() {
							that._MailDialog.close();
						},
						tooltip: "Cancelar",
						text: "Cancelar",
						width: "auto"
					}),
					new Button("btnSendMessages", {
						tooltip: "Enviar mensaje",
						press: [that.onSendMessage, that],
						text: "Enviar",
						type: "Accept",
						width: "auto"
					})
				],
				afterClose: function() {
					that._MailDialog.destroy();
				}
			}).addStyleClass("sapUiContentPadding");
			//Abrimos el dialogo
			this._MailDialog.open();
		},
		/**
		 * Función de ayuda para la selección de tipo de correo
		 * @param {sap.ui.base.Event} oEvent - Evento estándar Sapui5
		 * @private
		 */
		onSelectOptionItem: function(oEvent) {
			var sAlt = oEvent.getSource().getProperty("alt"),
				oThumb1 = this._getControl("thumbUp"),
				oThumb2 = this._getControl("thumbDown"),
				oThumb3 = this._getControl("lightbulb");

			function cambiarColores(oSelected, oUnselect1, oUnselect2) {
				oSelected.setProperty("color", "blue");
				oSelected.setProperty("hoverColor", "black");
				oUnselect1.setProperty("color", "black");
				oUnselect1.setProperty("hoverColor", "blue");
				oUnselect2.setProperty("color", "black");
				oUnselect2.setProperty("hoverColor", "blue");
			}
			//Verificamos Id
			switch (sAlt) {
				case "thumb-up":
					cambiarColores(oThumb1, oThumb2, oThumb3);
					this.opcionMensage = "1";
					break;
				case "thumb-down":
					cambiarColores(oThumb2, oThumb1, oThumb3);
					this.opcionMensage = "2";
					break;
				case "lightbulb":
					cambiarColores(oThumb3, oThumb1, oThumb2);
					this.opcionMensage = "3";
					break;
				default:
					MessageToast.show("Error, has ingresado en una función erronea");
					break;
			}
		},
		/**
		 * Función que prepara los datos al ERP para ser enviados como correo electrónico
		 * @pulic
		 */
		onSendMessage: function() {
			// Defimos variables y controles de elementos XML
			var emailHtml = {};
			var oCorreo = this._getControl("nameMessage");
			var oMensaje = this._getControl("descriptionMessage");
			// Validamos que los campo estén diligenciados
			if ((!this.opcionMensage) || (this.opcionMensage === false)) {
				MessageToast.show("No hay una calificación seleccionada.");
				return;
			} else if ((oCorreo.getValue() === "") || (oMensaje.getValue() === "")) {
				MessageToast.show("No has diligenciado todos los campos.");
				return;
			}
			emailHtml.Correo = oCorreo.getValue();
			emailHtml.Mensaje = oMensaje.getValue();
			emailHtml.Categoria = this.opcionMensage;
			// Realizamos el envío del Correo Html
			this.enviarCorreoHtml(emailHtml);
			// Cerrar Fragment
			this._MailDialog.close();
		},
		/**
		 * Función que lleva datos al ERP para ser enviados como correo electrónico
		 * @param {Array} oDataSend - Array con los datos a enviar en el erp
		 * @pulic
		 */
		enviarCorreoHtml: function(oDataSend) {
			// Creamos el modelo para realizar el Create
			var that = this;
			var oModel = new sap.ui.model.odata.ODataModel(that.sServiceUrl, true);
			//Llamamos a la función que realiza el create
			var oCreate = this.fnCreateEntity(oModel, that.sEntity, oDataSend);
			//Verificamos la respuesta del create
			if (oCreate.tipo === "S") {
				MessageToast.show("Se ha enviado correctamente el correo electrónico.");
				// Reiniciamos la variable de control
				this.opcionMensage = false;
			} else {
				sap.m.MessageBox.error(oCreate.msjs, null, "Mensaje del sistema", "OK", null);
			}
		},
		/**
		 * Obtener control del elemento sapui5 donde se encuentre
		 * @param {String} id - Element Id
		 * @returns {sap.m.AnyElement} Element Control
		 * @private
		 */
		_getControl: function(id) {
			var lmt = sap.ui.core.Core().byId(id);
			return lmt;
		},
		/**
		 * Consumir servicio CREATE.
		 * @public
		 * @param {object} pModelo hace referencia al modelo del servicio
		 * @param {string} pEntidad hace referencia a la entidad que se va a consumir
		 * @param {object} pDatoEndidad hace referencia a los dato a enviar a la entidad
		 * @returns {string} mensaje
		 */
		fnCreateEntity: function(pModelo, pEntidad, pDatoEndidad) {
			var vMensaje = null;
			var oMensaje = {};
			var fnSucess = function(data, response) {
				oMensaje.tipo = "S";
				oMensaje.datos = data;
			};
			var fnError = function(e) {
				vMensaje = JSON.parse(e.response.body);
				vMensaje = vMensaje.error.message.value;
				oMensaje.tipo = "E";
				oMensaje.msjs = vMensaje;
			};
			pModelo.create(pEntidad, pDatoEndidad, null, fnSucess, fnError, false);
			return oMensaje;
		}
	};
});